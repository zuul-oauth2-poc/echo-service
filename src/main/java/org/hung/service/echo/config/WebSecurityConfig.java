package org.hung.service.echo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.debug(true);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		  //.sessionManagement()
		  //  .sessionCreationPolicy(SessionCreationPolicy.NEVER)	  
		  //.and()
		    .authorizeRequests()
		    	.anyRequest()
		    		.authenticated();
	}
	
	/*
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
		  .inMemoryAuthentication()
		    .withUser("admin").password("{noop}abcd1234").authorities("USER","ADMIN");
	}
	*/
	
}
